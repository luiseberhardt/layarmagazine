package com.layar.magazinedemo;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.security.auth.callback.Callback;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.layar.sdk.LayarSDKClient;
import com.layar.sdk.LayarSDKFragment;
import com.layar.sdk.data.Action;
import com.layar.sdk.data.Layer;
import com.layar.sdk.data.Poi;
import com.layar.sdk.data.VisualSearchResult;

public class LayarSDKFragmentActivity extends FragmentActivity {
	public TextView text;

	private static final String TAG = Callback.class.getSimpleName();

	private LayarSDKFragment sdkFragment;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_layar_sdkfragment);

		TextView text = (TextView) findViewById(R.id.text);
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy 'at' HH:mm:ss");
		text.setText("Fragment Created: " + sdf.format(cal.getTime()));

		sdkFragment = (LayarSDKFragment) getSupportFragmentManager()
				.findFragmentById(R.id.sdk_fragment);
		sdkFragment.setSdkClient(sdkClient);
	}

	@Override
	protected void onStart() {
		super.onStart();
		sdkFragment.openScanMode();
	}

	private LayarSDKClient sdkClient = new LayarSDKClient() {

		public void appendTextView(String s) {
			text = (TextView) getSdkFragment().getActivity().findViewById(
					R.id.text);
			if ((text.getText().toString() == null)
					|| (text.getText().toString().isEmpty())) {
				text.setText(s);
			} 
			else {
				text.append(s);
				//text.setText(text.getText().toString()+s);
			}
		}

		@Override
		public void onActionTriggered(Layer layer, Poi poi, Action action) {
			Log.d(TAG, "onActionTriggered: " + layer);
			Log.d(TAG, "onActionTriggered: " + poi);
			Log.d(TAG, "onActionTriggered: " + action);
			appendTextView("onActionTriggered, layer: " + layer + " poi: "
					+ poi + " action: " + action + "\n\r ______ \n\r");
		}

		@Override
		public void onAugmentClicked(Layer layer, Poi poi) {
			Log.d(TAG, "onAugmentClicked: " + layer);
			Log.d(TAG, "onAugmentClicked: " + poi);
			appendTextView("onAugmentClicked, layer: " + layer + " poi: " + poi
					+ "\n\r ______ \n\r");
		}

		public void onAugmentFocused(Layer layer, Poi poi) {
			Log.d(TAG, "onAugmentFocused: " + layer);
			Log.d(TAG, "onAugmentFocused: " + poi);
			appendTextView("onAugmentFocused, layer: " + layer + " poi: " + poi
					+ "\n\r ______ \n\r");
		
		}

		@Override
		public void onAugmentShown(Layer layer, Poi poi) {
			Log.d(TAG, "onAugmentShown: " + layer);
			Log.d(TAG, "onAugmentShown: " + poi);
			appendTextView("onAugmentShown, layer: " + layer + " poi: " + poi
					+ "\n\r ______ \n\r");
		}

		@Override
		public boolean onBackPressed() {
			if (needsResetScanMode && !getSdkFragment().isInScanMode()) {
				needsResetScanMode = false;
				getSdkFragment().openScanMode();
		
				Log.d(TAG, "onBackPressed");
				appendTextView("onBackPressed" + "\n\r ______ \n\r");
				ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
				clipboard.setPrimaryClip((ClipData) text.getText());
				return true;
			}
		
			return false;
		}
		
		@Override
		public void onContentVisible(boolean contentVisible) {
			Log.d(TAG, "onContentVisible: " + contentVisible);			
			appendTextView("onContentVisible, contentVisible: " + String.valueOf(contentVisible)
					+ "\n\r ______ \n\r");
		}
		
		@Override
		public void onCreateOptionsMenu(Menu menu) {
			Log.d(TAG, "onCreateOptionsMenu: " + menu);
			appendTextView("onCreateOptionsMenu, menu: " + String.valueOf(menu)
					+ "\n\r ______ \n\r");
		}

		@Override
		public void onLayerLaunched(Layer layer, String s) {
			super.onLayerLaunched(layer, s);

			getSdkFragment().setPopOutFitContent(false);
			getSdkFragment().setPopOut3DModeEnabled(true);
			getSdkFragment().setPopOut3DTiltEnabled(true);
			
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat(
					"dd.MM.yyyy'at' HH:mm:ss");
			appendTextView("Layer launched: " + sdf.format(cal.getTime())
					+ "\n\r ______ \n\r");

			// FragmentActivity activity = getSdkFragment().getActivity();
			// FragmentManager fm = activity.getSupportFragmentManager();
			//
			// DialogFragment fragment = new DialogFragment();
			// fragment.show(activity.getSupportFragmentManager(), null);
		}

		// @Override
		// public void onActionTriggered(Layer layer, Poi poi, Action action) {
		// Toast.makeText(getSdkFragment().getActivity(),
		// "onActionTriggered:", Toast.LENGTH_SHORT).show();
		// }

		@Override
		public boolean onOptionsItemSelected(MenuItem item) {
			Log.d(TAG, "onCreateOptionsMenu: " + item);			
			appendTextView("onCreateOptionsMenu, item: " + String.valueOf(item)
					+ "\n\r ______ \n\r");
			return false;
		}
		
		public void onPopOutClosed(Layer layer,
				java.lang.String referenceImageId) {
			Log.d(TAG, "onPopOutClosed: " + layer);
			Log.d(TAG, "onPopOutClosed: " + referenceImageId);
			appendTextView("onPopOutClosed, layer: " + layer
					+ " referenceImageId: " + referenceImageId
					+ "\n\r ______ \n\r");		
		}

		public void onPopOutTriggered(Layer layer,
				java.lang.String referenceImageId) {
			Log.d(TAG, "onPopOutTriggered: " + layer);
			Log.d(TAG, "onPopOutTriggered: " + referenceImageId);
			appendTextView("onPopOutTriggered, layer: " + layer
					+ " referenceImageId: " + referenceImageId
					+ "\n\r ______ \n\r");
		
		}

		@Override
		public void onQRCodeRecognized(java.lang.String qrCodeData) {
			Log.d(TAG, "onQRCodeRecognized: " + qrCodeData);
			appendTextView("onQRCodeRecognized, qrCodeData: " + qrCodeData
					+ "\n\r ______ \n\r");
		}

		public void onReferenceImageLost(Layer layer,
				java.lang.String referenceImageId) {
			Log.d(TAG, "onReferenceImageLost: " + layer);
			Log.d(TAG, "onReferenceImageLost: " + referenceImageId);
			appendTextView("onReferenceImageLost, layer: " + layer
					+ " referenceImageId: " + referenceImageId
					+ "\n\r ______ \n\r");		
			// getSdkFragment().triggerPopOut(referenceImageId);
		
		}

		@Override
		public void onReferenceImageTracked(Layer layer,
				java.lang.String referenceImageId) {
			// getSdkFragment().openScreenshotMode();
			Log.d(TAG, "onReferenceImageTracked: " + layer);
			Log.d(TAG, "onReferenceImageTracked: " + referenceImageId);
			appendTextView("onReferenceImageTracked, layer: " + layer
					+ " image: " + referenceImageId + "\n\r ______ \n\r");
			getSdkFragment().closePopOut();

		}

		
		@Override
		public void onScreenshotModeClosed() {
			Log.d(TAG, "onScreenshotModeClosed");	
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat(
					"dd.MM.yyyy'at' HH:mm:ss");
			appendTextView("onScreenshotModeClosed: " + sdf.format(cal.getTime())
					+ "\n\r ______ \n\r");
		}
		
		@Override
		public void onScreenshotModeOpened() {
			Log.d(TAG, "onScreenshotModeOpened");	
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat(
					"dd.MM.yyyy'at' HH:mm:ss");
			appendTextView("onScreenshotModeOpened: " + sdf.format(cal.getTime())
					+ "\n\r ______ \n\r");
		}
		
		@Override
		public void onVideoPlaybackStarted(Layer layer, Poi poi) {
			Log.d(TAG, "onVideoPlaybackStarted: " + layer);
			Log.d(TAG, "onVideoPlaybackStarted: " + poi);
			appendTextView("onVideoPlaybackStarted, layer: " + layer + " poi: "
					+ poi + "\n\r ______ \n\r");
		}

		@Override
		public void onVisualSearchResult(int responseCode,
				java.lang.String responseMessage, VisualSearchResult[] results) {
			Log.d(TAG, "onVisualSearchResult: " + responseCode);
			Log.d(TAG, "onVisualSearchResult: " + responseMessage);
			Log.d(TAG, "onVisualSearchResult: " + results);
			appendTextView("onVisualSearchResult, responseCode: "
					+ responseCode + " responseMessage: " + responseMessage
					+ " results: " + results + "\n\r ______ \n\r");
		}

		public boolean shoudPerformVisualSearch(java.io.File queryImage) {
			Log.d(TAG, "shouldPerformVisualsearch: " + queryImage);
			appendTextView("shouldPerformVisualsearch, queryImage: "
					+ queryImage + "\n\r ______ \n\r");
			return true;
		}

		public boolean shouldShowNoResultsFound(java.io.File queryImage) {
			Log.d(TAG, "shouldShowNoResultsFound: " + queryImage);
			appendTextView("shouldShowNoResultsFound, queryImage: "
					+ queryImage + "\n\r ______ \n\r");
			return true;
		}

		private boolean needsResetScanMode = false;

		@Override
		public boolean shouldTriggerAction(Layer layer, Poi poi, Action action) {

			Uri uri = Uri.parse(action.getUri());
			if ("layar".equals(uri.getScheme())) {
				needsResetScanMode = true;

				String layerName = uri.getAuthority();
				getSdkFragment().openLayer(layerName);
				Log.d(TAG, "shouldTriggerAction: " + layer);
				Log.d(TAG, "shouldTriggerAction: " + poi);
				Log.d(TAG, "shouldTriggerAction: " + action);
				appendTextView("shouldTriggerAction, layer: " + layer
						+ " poi: " + poi + " action: " + action
						+ "\n\r ______ \n\r");
				return false;
			}

			return true;
		}

	};
}
